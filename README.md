# Blockchain projet 

## 1- Contexte du projet 

La technologie Blockchain est de plus en plus utilisée dans de nombreuses thématiques. Il est
donc primordial de s’y intéresser en tant que développeur et de comprendre à minima les
mécaniques de base. Vous devrez donc, à l’aide des indications que vous trouverez ci-dessous,
réaliser une ébauche de blockchain fonctionnelle.

## 2- Langage de programmation

Ce projet a été développer dans le langage python car il est réputé pour sa
rapidité de réalisation de calculs.

## 3- Architecture du projet

<pre>
blockchain
 ├── classes
 │   └── Block.py
 │   └── Chain.py
 │   └── Wallet.py
 ├── content
 │   └── blocs
 │       └── stockage de l’ensemble informations contenus dans les
blocs
 │   └── wallets
 │       └── stockage de l’ensemble des wallets et des informations
afférentes
 ├── settings
 │   └── settings.py
 ├── main.py
 └── README.md
</pre>
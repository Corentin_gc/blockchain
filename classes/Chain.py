import hashlib
import string
import random
from .Block import Block
from .Wallet import Wallet


class Chain:
    def __init__(self):
        self.blocks = []
        self.last_transaction_number = 0

    def __repr__(self):
        return f'Chain(\n' \
               f'name={self.blocks},\n ' \
               f'last_transaction_number={self.last_transaction_number}\n)'

    def generate_hash(self):
        chars = string.ascii_uppercase + string.digits
        generate_string = ""
        index = random.randint(1, 101)

        for i in range(index):
            generate_string = generate_string.join(random.choice(chars))

        hash_generate = hashlib.sha256(generate_string.encode()).hexdigest()

        if self.verify_hash(hash_generate):
            self.add_block(generate_string, hash_generate)
            return hash_generate
        else:
            self.generate_hash()

    def verify_hash(self, hash_generate):
        # if not hash_generate.startswith("0000"):
        #     return False

        for block in self.blocks:
            if block.hash == hash_generate:
                return False

        return True

    def add_block(self, generate_string, hash_generate):
        if len(self.blocks) < 1:
            parent_hash = "00"
        else:
            parent_hash = self.blocks[len(self.blocks) - 1].hash

        block = Block(generate_string, hash_generate, parent_hash)
        block.save()

        self.blocks.append(block)

    def get_block(self, hash_block):
        for block in self.blocks:
            if block.hash == hash_block:
                return block

    def add_transaction(self, wallet_emitter, wallet_receiver, price):
        emitter = Wallet(wallet_emitter)
        if emitter is not False:
            if emitter.balance >= price:
                receiver = Wallet(wallet_receiver)
                if receiver is not False:
                    block = self.blocks[len(self.blocks) - 1]
                    if block.get_weight():
                        self.last_transaction_number += 1

                        block.add_transactions(self.last_transaction_number, wallet_emitter, wallet_receiver, price)

                        emitter.sub_balance(wallet_emitter, wallet_receiver, price)
                        receiver.add_balance(wallet_receiver, wallet_emitter, price)

    def find_transaction(self, transaction_number):
        for block in self.blocks:
            for transaction in block.transactions:
                if transaction["transaction_number"] == transaction_number:
                    return block

    def get_last_transaction_number(self):
        return str(self.last_transaction_number)

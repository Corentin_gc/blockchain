import os
import uuid
import json


class Wallet:
    def __init__(self, unique_id=None):
        if unique_id is None:
            self.unique_id = self.generate_unique_id()
            self.balance = 100
            self.history = []
            self.save()
        else:
            self.unique_id = unique_id
            self.load()

    def __repr__(self):
        return f'Wallet(\n' \
               f'unique_id={self.unique_id},\n ' \
               f'balance={self.balance},\n ' \
               f'history={self.history}\n)'

    def generate_unique_id(self):
        unique_id_generate = str(uuid.uuid4())
        return self.check_unique_id(unique_id_generate)

    def check_unique_id(self, unique_id_generate):
        if os.path.isfile(f"./content/wallets/{unique_id_generate}.json"):
            return self.generate_unique_id()
        else:
            return unique_id_generate

    def add_balance(self, receiver, emitter, price):
        self.balance += price
        self.send(emitter, receiver, price)

    def sub_balance(self, emitter, receiver, price):
        self.balance -= price
        self.send(emitter, receiver, price)

    def send(self, emitter, receiver, price):
        history_transaction = {
            "emitter": emitter,
            "receiver": receiver,
            "price": price,
        }
        self.history.append(history_transaction)
        self.save()

    def save(self):
        data = {
            "unique_id": self.unique_id,
            "balance": self.balance,
            "history": [],
        }

        for h in self.history:
            data["history"].append(h)

        with open(f"./content/wallets/{self.unique_id}.json", "w") as f:
            json.dump(data, f)

    def load(self):
        if os.path.isfile(f"./content/wallets/{self.unique_id}.json"):
            with open(f"./content/wallets/{self.unique_id}.json") as f:
                data = json.load(f)
                for _ in data:
                    self.unique_id = data["unique_id"]
                    self.balance = data["balance"]
                    self.history = data["history"]
        else:
            return False

import json
import os
import hashlib


class Block:
    def __init__(self, base_hash, new_hash, parent_hash):
        self.base_hash = base_hash
        self.hash = new_hash
        self.parent_hash = parent_hash
        self.transactions = []
        self.check_hash()

    def __repr__(self):
        return f'Block(\n' \
               f'base_hash={self.base_hash},\n ' \
               f'hash={self.hash},\n ' \
               f'parent_hash={self.parent_hash},\n ' \
               f'transactions={self.transactions}\n) '

    def check_hash(self):
        check_hash = hashlib.sha256(self.base_hash.encode()).hexdigest()
        if check_hash == self.hash:
            return True
        else:
            return False

    def add_transactions(self, transaction_number, receiver, emitter, price):
        data_transaction = {
            "transaction_number": transaction_number,
            "emitter": emitter,
            "receiver": receiver,
            "price": price,
        }

        self.transactions.append(data_transaction)
        self.save()

    def get_transactions(self, transactions_number):
        for t in self.transactions:
            if t == transactions_number:
                return t

    def get_weight(self):
        path = f"./content/blocs/{self.hash}.json"
        if os.path.isfile(path):
            if os.path.getsize(path) < 256000:
                return True
            else:
                return False
        else:
            return False

    def save(self):
        data = {
            "hash": self.hash,
            "base_hash": self.base_hash,
            "parent_hash": self.parent_hash,
            "transactions": [],
        }

        for t in self.transactions:
            data["transactions"].append(t)

        with open(f"./content/blocs/{self.hash}.json", "w") as f:
            json.dump(data, f)

    def load(self):
        with open(f"./content/blocs/{self.hash}.json") as f:
            data = json.load(f)
            for _ in data:
                self.hash = data["hash"]
                self.base_hash = data["base_hash"]
                self.parent_hash = data["parent_hash"]
                self.transactions = data["transactions"]

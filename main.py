from classes.Chain import Chain
from classes.Wallet import Wallet


def main():
    new_chain = Chain()
    new_chain.generate_hash()

    first_wallet = Wallet()
    print("first_wallet : ")
    print(first_wallet.unique_id)
    print(first_wallet.balance)

    second_wallet = Wallet()
    print("second_wallet : ")
    print(second_wallet.unique_id)
    print(second_wallet.balance)

    new_chain.add_transaction(first_wallet.unique_id, second_wallet.unique_id, 50)
    first_wallet.load()
    second_wallet.load()
    print("first_wallet : ")
    print(first_wallet.balance)
    print("second_wallet : ")
    print(second_wallet.balance)

    print(new_chain.blocks)
    print(new_chain.get_last_transaction_number())
    print(new_chain.find_transaction(1))


if __name__ == "__main__":
    main()
